----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03.01.2020 13:07:13
-- Design Name: 
-- Module Name: sync_debouncer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sync_debouncer is
    Port ( 
           sync_deb_in : in STD_LOGIC;
           clk : in STD_LOGIC;
           rst : in STD_LOGIC;--entrada de reset necesaria en caso de que se desee resetear al debouncer 
           sync_deb_out : out STD_LOGIC);
end sync_debouncer;

architecture Behavioral of sync_debouncer is
    signal sync_out : std_logic;--Se�al interna para comunicar la salida del sincronizador con el antirrebotes
    signal Q1, Q2, Q3 : std_logic;--Se�ales auxiliares que permiten evaluar siempre los valores anteriores de la se�al(debouncer)
    constant SYNC_STAGES : integer := 3;
    constant PIPELINE_STAGES : integer := 1;
    constant INIT : std_logic := '0';
    signal sreg : std_logic_vector(SYNC_STAGES-1 downto 0) := (others => INIT);
    attribute async_reg : string;
    attribute async_reg of sreg : signal is "true";
    signal sreg_pipe : std_logic_vector(PIPELINE_STAGES-1 downto 0) := (others => INIT);
    attribute shreg_extract : string;
    attribute shreg_extract of sreg_pipe : signal is "false";
begin
process(clk)
begin
    if(clk'event and clk='1')then
        sreg <= sreg(SYNC_STAGES-2 downto 0) & sync_deb_in;
    end if;
end process;

no_pipeline : if PIPELINE_STAGES = 0 generate
begin
    sync_out <= sreg(SYNC_STAGES-1);
end generate;

one_pipeline : if PIPELINE_STAGES = 1 generate
begin
    process(clk)
    begin
        if(clk'event and clk='1') then
            sync_out <= sreg(SYNC_STAGES-1);
        end if;
    end process;
end generate;

multiple_pipeline : if PIPELINE_STAGES > 1 generate
begin
    process(clk)
    begin
        if(clk'event and clk='1') then
            sreg_pipe <= sreg_pipe(PIPELINE_STAGES-2 downto 0) & sreg(SYNC_STAGES-1);
        end if;
    end process;
sync_out <= sreg_pipe(PIPELINE_STAGES-1);
end generate;

process(clk)--se obliga a entrar con cada cambio de reloj
 begin
        if (clk'event and clk = '1') then --ante el flanco de subida del reloj
            if (rst = '0') then --en caso de que la se�eal reset est� activa a (nivel bajo)
             Q1 <= '0';-- asigna ceros en caso de que est� reseteado
             Q2 <= '0';-- asigna ceros en caso de que est� reseteado
             Q3 <= '0';-- asigna ceros en caso de que est� reseteado
            else
             Q1 <= sync_out;--le asigna a Q1 el valor actual 
             Q2 <= Q1;--le asigna a Q2 el valor de Q1 que como no ha sido actualizado es el que tuviera de la iteraci�n anerior
             Q3 <= Q2;--le asigna a Q3 el valor de Q2 que como no ha sido actualizado es el que tuviera de la iteraci�n anerior
            end if;
       end if;
end process;
  sync_deb_out <= Q1 and Q2 and (not Q3);--saca por la salida la l�gica tal que si el actual y el anterior son uno y antes 
  --ten�an un cero considera que ya se ha alcanzado el r�gimen permanente

end Behavioral;

