----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 15.12.2019 10:38:56
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top is

end TOP;


architecture BENCH of top is

    component sync_debouncer
      Port ( 
             sync_deb_in : in STD_LOGIC;
             clk : in STD_LOGIC;
             rst : in STD_LOGIC;
             sync_deb_out : out STD_LOGIC);
  end component;

  signal sync_deb_in: STD_LOGIC;
  signal clk: STD_LOGIC;
  signal rst: STD_LOGIC;
  signal sync_deb_out: STD_LOGIC;

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  uut: sync_debouncer port map ( sync_deb_in  => sync_deb_in,
                                 clk          => clk,
                                 rst          => rst,
                                 sync_deb_out => sync_deb_out );

  stimulus: process
  
  begin
  
    -- Put initialisation code here
     sync_deb_in <= '0';
     rst <= '1';
  
    -- Put test bench stimulus code here
    wait for 1.69*clock_period;
    
    --rst<='1';
    for j in 1 to 2 loop
        sync_deb_in<= '1';
         wait for 10*clock_period;
        for i in 1 to 100 loop
            wait for 0.1*clock_period;
            sync_deb_in<= not sync_deb_in;
        end loop;
        sync_deb_in<= '0';
        wait for 15*clock_period;
       end loop;
   -- rst<='0';
    wait for 5.5*clock_period;
    --rst<='1';
    
    --clk<=not clk after 0.5*clock_period;
    sync_deb_in<='0';
    wait for 20*clock_period;
    stop_the_clock <= true;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;
  END;