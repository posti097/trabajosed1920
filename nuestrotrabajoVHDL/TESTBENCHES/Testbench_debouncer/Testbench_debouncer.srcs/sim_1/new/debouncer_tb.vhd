library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity Debouncer_tb is
end;

architecture bench of Debouncer_tb is

  component Debouncer
      Port ( clk : in STD_LOGIC;
             rst : in STD_LOGIC;
             btn_in : in STD_LOGIC;
             btn_out : out STD_LOGIC);
  end component;

  signal clk: STD_LOGIC;
  signal rst: STD_LOGIC;
  signal btn_in: STD_LOGIC;
  signal btn_out: STD_LOGIC;

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  uut: Debouncer port map ( clk     => clk,
                            rst     => rst,
                            btn_in  => btn_in,
                            btn_out => btn_out );

  stimulus: process
  begin
  
    -- Put initialisation code here
    btn_in<='0';
    rst<='0';
    wait for 100ns;
    rst<='1';
    wait for 10ns;
    btn_in<='1';
    wait for 200ns;
    for i in 0 to 50 loop
        btn_in<=not btn_in;
        wait for 0.65ns;
    end loop;
    wait for 20ns;
    btn_in<='1';
    wait for 100ns;

    -- Put test bench stimulus code here

    stop_the_clock <= true;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;