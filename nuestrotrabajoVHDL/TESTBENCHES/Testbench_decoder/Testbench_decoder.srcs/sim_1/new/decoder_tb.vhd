library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity Decoder_tb is
end;

architecture bench of Decoder_tb is

  component Decoder
      Port ( code: in STD_LOGIC_VECTOR(0 TO 3);
             led : out STD_LOGIC_VECTOR (6 DOWNTO 0));
  end component;

  signal code: STD_LOGIC_VECTOR(0 TO 3);
  signal led: STD_LOGIC_VECTOR (6 DOWNTO 0);

begin

  uut: Decoder port map ( code => code,
                          led  => led );

  stimulus: process
  begin
    -- Put initialisation code here
  
    -- Put test bench stimulus code here
    for i in 0 to 10 loop
        case i is
        when 0=> code<="0000";
        when 1=> code<="0001";
        when 2=> code<="0010";
        when 3=> code<="0011";
        when 4=> code<="0100";
        when 5=> code<="0101";
        when 6=> code<="0110";
        when 7=> code<="0111";
        when 8=> code<="1000";
        when 9=> code<="1001";
        when 10=> code<="1010";
        end case;
        wait for 100 ns;
    end loop;
    wait;
  end process;


end;