library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity div_frec_tb is
end;

architecture bench of div_frec_tb is

  component div_frec
  Generic (N: positive:= 500);
      Port ( clk : in STD_LOGIC;
             clk_div : out STD_LOGIC);
  end component;

  signal clk: STD_LOGIC;
  signal clk_div: STD_LOGIC;

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  -- Insert values for generic parameters !!
  uut: div_frec generic map ( N       =>  15)
                   port map ( clk     => clk,
                              clk_div => clk_div );

  stimulus: process
  begin
  
    -- Put initialisation code here
    wait for 900ns;

    -- Put test bench stimulus code here

    stop_the_clock <= true;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;