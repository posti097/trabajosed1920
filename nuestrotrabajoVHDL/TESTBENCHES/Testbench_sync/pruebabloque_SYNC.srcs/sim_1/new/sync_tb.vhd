library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity sync_tb is
end;

architecture bench of sync_tb is

  component sync
      Port ( sync_in : in STD_LOGIC;
             clk : in STD_LOGIC;
             sync_out : out STD_LOGIC);
  end component;

  signal sync_in: STD_LOGIC;
  signal clk: STD_LOGIC;
  signal sync_out: STD_LOGIC;

  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  uut: sync port map ( sync_in  => sync_in,
                       clk      => clk,
                       sync_out => sync_out );

  stimulus: process
  begin
  
    -- Put initialisation code here
    sync_in<='1';
    -- Put test bench stimulus code here
    wait for 1.69*clock_period;
    for i in 1 to 20 loop
    	wait for 10ns;
    	sync_in <='1';
    	wait for 20ns;
    	sync_in <='0';
    end loop;
    stop_the_clock <= true;
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;