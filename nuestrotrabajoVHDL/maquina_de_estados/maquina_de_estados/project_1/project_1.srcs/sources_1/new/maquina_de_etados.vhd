----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 17.01.2020 18:45:12
-- Design Name: 
-- Module Name: maquina_de_etados - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity maquina_de_etados is
Port  ( count : in std_logic_vector (2 downto 0); --3 de momento
        admin , start , reset , clk : in std_logic;
        sw : in std_logic_vector (12 downto 0);
        salida: out std_logic_vector (1 downto 0)
);
end maquina_de_etados;

architecture Behavioral of maquina_de_etados is
type estado is (reposo ,administrador , comprobacion , respuesta );
signal ahora , siguiente : estado;
signal contrasena: unsigned (sw'range);
signal solution: std_logic; --bit que indica en la comprobacion si es buena
                            --1, si es correcto. 0, si es falso
begin

contrasena<=to_unsigned(0,contrasena'length); --contraseņa predefinida son todos 0
solution<= '0'; --respuesta inicial es 0

cambio: process( clk , reset)

begin
if reset= '0' then -- reset asincrono a nivel bajo
 ahora <= reposo;
 elsif clk'event and clk= '1' then --se actualiza el estado
    ahora<= siguiente;
 end if;
end process;

maq: process (ahora, admin , start)

begin
siguiente <=ahora;
case ahora is 
    when reposo =>
        if admin= '1' then
            siguiente<= administrador;
        elsif start='1' and admin= '0' then
            siguiente<= comprobacion;
        end if;
    when administrador =>
        if admin='0' then
            contrasena<= unsigned(sw);
            siguiente<= reposo;
        end if;
    when comprobacion =>
        if start = '0' then
            if contrasena=unsigned(sw)then
                solution<='1';
            else
                solution<='0';
            end if;
            siguiente<= respuesta;
        end if;
    when respuesta =>
        if reset= '0' then
            siguiente<=reposo;
        end if;
        --no hace falta volver de respuesta a reposo ya que lo realiza el anterior process
    end case;
end process;

iluminacion: process(ahora)
begin

if ahora=respuesta then
    salida<='1' & solution; --el primer bit indica si esta en respuesta (entonces hay que encender las luces de salida.
                            --el segundo bit indica si esta correcto la salida. 1=correcto y 0=erroneo
else
    salida<=(others=>'0'); --si la salida son todos ceros es que no esta en modo salida y no hay que encender luces
end if;
end process;
end Behavioral;
