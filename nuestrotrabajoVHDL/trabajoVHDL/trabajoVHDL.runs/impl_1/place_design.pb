
Q
Command: %s
53*	vivadotcl2 
place_design2default:defaultZ4-113h px� 
�
@Attempting to get a license for feature '%s' and/or device '%s'
308*common2"
Implementation2default:default2
xc7k70t2default:defaultZ17-347h px� 
�
0Got license for feature '%s' and/or device '%s'
310*common2"
Implementation2default:default2
xc7k70t2default:defaultZ17-349h px� 
P
Running DRC with %s threads
24*drc2
22default:defaultZ23-27h px� 
V
DRC finished with %s
79*	vivadotcl2
0 Errors2default:defaultZ4-198h px� 
e
BPlease refer to the DRC report (report_drc) for more information.
80*	vivadotclZ4-199h px� 
p
,Running DRC as a precondition to command %s
22*	vivadotcl2 
place_design2default:defaultZ4-22h px� 
P
Running DRC with %s threads
24*drc2
22default:defaultZ23-27h px� 
V
DRC finished with %s
79*	vivadotcl2
0 Errors2default:defaultZ4-198h px� 
e
BPlease refer to the DRC report (report_drc) for more information.
80*	vivadotclZ4-199h px� 
U

Starting %s Task
103*constraints2
Placer2default:defaultZ18-103h px� 
}
BMultithreading enabled for place_design using a maximum of %s CPUs12*	placeflow2
22default:defaultZ30-611h px� 
v

Phase %s%s
101*constraints2
1 2default:default2)
Placer Initialization2default:defaultZ18-101h px� 
�

Phase %s%s
101*constraints2
1.1 2default:default29
%Placer Initialization Netlist Sorting2default:defaultZ18-101h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2
00:00:002default:default2
1385.3522default:default2
0.0002default:defaultZ17-268h px� 
Z
EPhase 1.1 Placer Initialization Netlist Sorting | Checksum: ffd59f7a
*commonh px� 
�

%s
*constraints2s
_Time (s): cpu = 00:00:00 ; elapsed = 00:00:00.007 . Memory (MB): peak = 1385.352 ; gain = 0.0002default:defaulth px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2
00:00:002default:default2
1385.3522default:default2
0.0002default:defaultZ17-268h px� 
�

Phase %s%s
101*constraints2
1.2 2default:default2F
2IO Placement/ Clock Placement/ Build Placer Device2default:defaultZ18-101h px� 
�
[Partially locked IO Bus is found. Following components of the IO Bus %s are not locked: %s
87*place2
digsel2default:default2�
� '<MSGMETA::BEGIN::BLOCK>digsel[7]<MSGMETA::END>'  '<MSGMETA::BEGIN::BLOCK>digsel[5]<MSGMETA::END>'  '<MSGMETA::BEGIN::BLOCK>digsel[3]<MSGMETA::END>'  '<MSGMETA::BEGIN::BLOCK>digsel[2]<MSGMETA::END>'  '<MSGMETA::BEGIN::BLOCK>digsel[1]<MSGMETA::END>'  '<MSGMETA::BEGIN::BLOCK>digsel[0]<MSGMETA::END>' "
	digsel[7]2 ':'  '"
	digsel[5]:'  '"
	digsel[3]:'  '"
	digsel[2]:'  '"
	digsel[1]:'  '"
	digsel[0]:' 2default:default8Z30-87h px� 
�
[Partially locked IO Bus is found. Following components of the IO Bus %s are not locked: %s
87*place2
segment2default:default2�
� '<MSGMETA::BEGIN::BLOCK>segment[6]<MSGMETA::END>'  '<MSGMETA::BEGIN::BLOCK>segment[5]<MSGMETA::END>'  '<MSGMETA::BEGIN::BLOCK>segment[3]<MSGMETA::END>'  '<MSGMETA::BEGIN::BLOCK>segment[2]<MSGMETA::END>'  '<MSGMETA::BEGIN::BLOCK>segment[1]<MSGMETA::END>' "

segment[6]2 ':'  '"

segment[5]:'  '"

segment[3]:'  '"

segment[2]:'  '"

segment[1]:' 2default:default8Z30-87h px� 
�
[Partially locked IO Bus is found. Following components of the IO Bus %s are not locked: %s
87*place2
sw2default:default2�
� '<MSGMETA::BEGIN::BLOCK>sw[1]<MSGMETA::END>'  '<MSGMETA::BEGIN::BLOCK>sw[2]<MSGMETA::END>'  '<MSGMETA::BEGIN::BLOCK>sw[3]<MSGMETA::END>'  '<MSGMETA::BEGIN::BLOCK>sw[6]<MSGMETA::END>'  '<MSGMETA::BEGIN::BLOCK>sw[7]<MSGMETA::END>'  '<MSGMETA::BEGIN::BLOCK>sw[8]<MSGMETA::END>'  '<MSGMETA::BEGIN::BLOCK>sw[9]<MSGMETA::END>'  '<MSGMETA::BEGIN::BLOCK>sw[11]<MSGMETA::END>' "
sw[1]2 ':'  '"
sw[2]:'  '"
sw[3]:'  '"
sw[6]:'  '"
sw[7]:'  '"
sw[8]:'  '"
sw[9]:'  '"
sw[11]:' 2default:default8Z30-87h px� 
E
%Done setting XDC timing constraints.
35*timingZ38-35h px� 
�	
9Poor placement for routing between an IO pin and BUFG. %s528*place2�
�If this sub optimal condition is acceptable for this design, you may use the CLOCK_DEDICATED_ROUTE constraint in the .xdc file to demote this message to a WARNING. However, the use of this override is highly discouraged. These examples can be used directly in the .xdc file to override this clock rule.
	< set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets clk_IBUF] >

	<MSGMETA::BEGIN::BLOCK>clk_IBUF_inst<MSGMETA::END> (IBUF.O) is locked to IPAD_X0Y36
	<MSGMETA::BEGIN::BLOCK>clk_IBUF_BUFG_inst<MSGMETA::END> (BUFG.I) is provisionally placed by clockplacer on BUFGCTRL_X0Y31
"�
clk_IBUF_inst2�If this sub optimal condition is acceptable for this design, you may use the CLOCK_DEDICATED_ROUTE constraint in the .xdc file to demote this message to a WARNING. However, the use of this override is highly discouraged. These examples can be used directly in the .xdc file to override this clock rule.
	< set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets clk_IBUF] >

	:# (IBUF.O) is locked to IPAD_X0Y36
	"[
clk_IBUF_BUFG_inst:C (BUFG.I) is provisionally placed by clockplacer on BUFGCTRL_X0Y31
2default:default8Z30-574h px� 
h
SPhase 1.2 IO Placement/ Clock Placement/ Build Placer Device | Checksum: 13a6eed55
*commonh px� 
�

%s
*constraints2o
[Time (s): cpu = 00:00:02 ; elapsed = 00:00:02 . Memory (MB): peak = 1385.352 ; gain = 0.0002default:defaulth px� 
I
4Phase 1 Placer Initialization | Checksum: 13a6eed55
*commonh px� 
�

%s
*constraints2o
[Time (s): cpu = 00:00:02 ; elapsed = 00:00:02 . Memory (MB): peak = 1385.352 ; gain = 0.0002default:defaulth px� 
�
�Placer failed with error: '%s'
Please review all ERROR and WARNING messages during placement to understand the cause for failure.
1*	placeflow2*
IO Clock Placer failed2default:defaultZ30-99h px� 
>
)Ending Placer Task | Checksum: 13a6eed55
*commonh px� 
�

%s
*constraints2o
[Time (s): cpu = 00:00:02 ; elapsed = 00:00:02 . Memory (MB): peak = 1385.352 ; gain = 0.0002default:defaulth px� 
Z
Releasing license: %s
83*common2"
Implementation2default:defaultZ17-83h px� 
�
G%s Infos, %s Warnings, %s Critical Warnings and %s Errors encountered.
28*	vivadotcl2
422default:default2
182default:default2
392default:default2
32default:defaultZ4-41h px� 
N

%s failed
30*	vivadotcl2 
place_design2default:defaultZ4-43h px� 
m
Command failed: %s
69*common28
$Placer could not place all instances2default:defaultZ17-69h px� 
�
Exiting %s at %s...
206*common2
Vivado2default:default2,
Mon Jan 20 17:06:39 20202default:defaultZ17-206h px� 


End Record