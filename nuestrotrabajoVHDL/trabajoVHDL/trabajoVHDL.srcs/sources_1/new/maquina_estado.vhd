----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 20.01.2020 12:39:17
-- Design Name: 
-- Module Name: maquina_estado - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity maquina_estado is
Port  (
        admin , start , reset , clk : in std_logic;
        sw : in std_logic_vector (12 downto 0);
        salida: out std_logic_vector (1 downto 0)
);
end maquina_estado;

architecture Behavioral of maquina_estado is
type estado_t is (reposo ,administrador , comprobacion , respuesta_correcta , respuesta_incorrecta );
signal estado, prx_estado : estado_t;
signal contrasena, prx_contrasena: std_logic_vector (sw'range);
--signal solution: std_logic; --bit que indica en la comprobacion si es buena
                            --1, si es correcto. 0, si es falso
begin
--contrasena<=(others=>'0'); --contraseņa predefinida son todos 0
--solution<= '0'; --respuesta inicial es 0
  reg_estado: process( clk , reset)
  begin
  if reset= '0' then -- reset asincrono a nivel bajo
     estado <= reposo;
   elsif clk'event and clk= '1' then --se actualiza el estado
     estado <= prx_estado;
     contrasena <= prx_contrasena;
   end if;
  end process;

  dec_prx_estado: process (admin , contrasena, estado, reset, start, sw)
  begin
    prx_estado    <= estado;
    prx_contrasena <= contrasena;
    case estado is 
      when reposo =>
        if admin = '1' then
          prx_estado <= administrador;
        elsif start = '1' then
          prx_estado <= comprobacion;
        end if;
      when administrador =>
        if admin = '0' then
          prx_contrasena <= sw;
          prx_estado <= reposo;
        end if;
      when comprobacion =>
        if start = '0' then
          if contrasena = sw then
            prx_estado <= respuesta_correcta;
          else
            prx_estado<= respuesta_incorrecta;
          end if;    
        end if;
      when respuesta_correcta =>
        prx_estado <= respuesta_correcta;
          --no hace falta volver de respuesta a reposo ya que lo realiza el anterior process
      when respuesta_incorrecta =>
        prx_estado <= respuesta_incorrecta;
    end case;
  end process;

  decod_salida: process(estado)
  begin
    case estado is
      when respuesta_correcta =>
        salida<="11"; --el primer bit indica si esta en respuesta (entonces hay que encender las luces de salida.
                      --el segundo bit indica si esta correcto la salida. 1=correcto y 0=erroneo
      when respuesta_incorrecta => 
        salida<="10"; --si la salida son todos ceros es que no esta en modo salida y no hay que encender luces
      when others =>
        salida <= (others=>'0');
    end case;
  end process;
end Behavioral;
