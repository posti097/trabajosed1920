----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 15.12.2019 10:38:56
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top is
 GENERIC(NB:INTEGER:=2; NSW:INTEGER:=13);
 
    Port ( clk : in STD_LOGIC;
          -- b: in STD_LOGIC_VECTOR(0 TO NB - 1);
           sw: in STD_LOGIC_VECTOR(0 TO NSW - 1);         
           reset : in STD_LOGIC;
           admin : in STD_LOGIC;
           start : in STD_LOGIC;
           led_correcto: out STD_LOGIC;
           led_incorrecto: out STD_LOGIC;
           segment : out STD_LOGIC_VECTOR (6 downto 0);
           digsel : out STD_LOGIC_VECTOR (7 downto 0));--El digsel(4) va al primero de la siguiente fila
end TOP;


architecture Behavioral of top is

    --Se�ales intermedias para sincronizar las entradas:
      SIGNAL b_sync_debouncer: STD_LOGIC_VECTOR(0 TO NB-1);
     SIGNAL sw_sync: STD_LOGIC_VECTOR(0 TO NSW-1);
     SIGNAL reset_sync, admin_sync, start_sync: STD_LOGIC;
     SIGNAL salida: STD_LOGIC_VECTOR(1 DOWNTO 0);
    
     component sync_debouncer is
     Port ( 
               sync_deb_in : in STD_LOGIC;
               rst : in STD_LOGIC;--entrada de reset necesaria en caso de que se desee resetear al debouncer. DEBE ESTAR SINCRONIZADA
               clk : in STD_LOGIC;
               sync_deb_out : out STD_LOGIC);
     end component;
     
      component sync is
         Port ( 
               sync_in : in STD_LOGIC;
               clk : in STD_LOGIC;
               sync_out : out STD_LOGIC);
         end component;
    
    component maquina_estado is
        Port(
            admin , start , reset , clk : in std_logic;
            sw : in std_logic_vector (12 downto 0);
            salida: out std_logic_vector (1 downto 0)
        );
        end component;
        
    component Display is
        Port(
         r_or_e: in STD_LOGIC_VECTOR (1 downto 0):= "00";--Variable que determina si es error o right
           clk: in STD_LOGIC := '0';
           segment : out STD_LOGIC_VECTOR (6 downto 0) := "1111111";
           digsel : out STD_LOGIC_VECTOR (7 downto 0) := "11111111";--El digsel(4) va al primero de la siguiente fila
           led_correcto: out STD_LOGIC;
           led_incorrecto: out STD_LOGIC
        );
    end component;
begin
     --Instanciaciones sincronizadores y antirebotes de se�ales (sync_debouncer):
   

    inst_sync_sw0: sync 
    PORT MAP (
        sync_in => SW(0),
        clk => clk,
        sync_out => sw_sync(0)
    );
    
    inst_sync_sw1: sync 
    PORT MAP (
        sync_in => SW(1),
        clk => clk,
        sync_out => sw_sync(1)
    );
    
    inst_sync_sw2: sync 
    PORT MAP (
        sync_in => SW(2),
        clk => clk,
        sync_out => sw_sync(2)
    );
    
    inst_sync_sw3: sync 
    PORT MAP (
        sync_in => SW(3),
        clk => clk,
        sync_out => sw_sync(3)
    );
    
    inst_sync_sw4: sync 
    PORT MAP (
        sync_in => SW(4),
        clk => clk,
        sync_out => sw_sync(4)
    );
    
    inst_sync_sw5: sync 
    PORT MAP (
        sync_in => SW(5),
        clk => clk,
        sync_out => sw_sync(5)
    );
    
    inst_sync_sw6: sync 
    PORT MAP (
        sync_in => SW(6),
        clk => clk,
        sync_out => sw_sync(6)
    );
    
    inst_sync_sw7: sync 
    PORT MAP (
        sync_in => SW(7),
        clk => clk,
        sync_out => sw_sync(7)
    );
    
    inst_sync_sw8: sync 
    PORT MAP (
        sync_in => SW(8),
        clk => clk,
        sync_out => sw_sync(8)
    );
    
    inst_sync_sw9: sync 
    PORT MAP (
        sync_in => SW(9),
        clk => clk,
        sync_out => sw_sync(9)
    );
    
    inst_sync_sw10: sync 
    PORT MAP (
        sync_in => SW(10),
        clk => clk,
        sync_out => sw_sync(10)
    );
    
    inst_sync_sw11: sync 
    PORT MAP (
        sync_in => SW(11),
        clk => clk,
        sync_out => sw_sync(11)
    );
    
    inst_sync_sw12: sync 
    PORT MAP (
        sync_in => SW(12),
        clk => clk,
        sync_out => sw_sync(12)
    );
    
    inst_sync_admin: sync
    PORT MAP (
        sync_in => admin,
        clk => clk,
        sync_out =>admin_sync
    );
    
     inst_sync_start: sync 
    PORT MAP (
        sync_in => start,
        clk => clk,
        sync_out =>start_sync
    );
    
    
   --para el reset no hay debouncer, ya que el reset es una de las entradas del debouncer 
    inst_sync_reset: sync 
    PORT MAP (
        sync_in => reset,
        clk => clk,
        sync_out =>reset_sync
    );

    Maquina_de_estados: maquina_estado
    PORT MAP (
        admin=> admin_sync,
        start=> start_sync,
        reset=> reset_sync,
        clk=> clk,
        sw(0)=>sw_sync(0),
        sw(1)=>sw_sync(1),
        sw(2)=>sw_sync(2),
        sw(3)=>sw_sync(3),
        sw(4)=>sw_sync(4),
        sw(5)=>sw_sync(5),
        sw(6)=>sw_sync(6),
        sw(7)=>sw_sync(7),
        sw(8)=>sw_sync(8),
        sw(9)=>sw_sync(9),
        sw(10)=>sw_sync(10),
        sw(11)=>sw_sync(11),
        sw(12)=>sw_sync(12),
        salida=>salida
        );

        
        
        
    displays: Display
    Port MAP (
         r_or_e=>salida,
         clk=>clk,
         segment=>segment,
         digsel=>digsel,
         led_correcto=>led_correcto,
         led_incorrecto=>led_incorrecto
  );
end Behavioral;
